﻿using UnityEngine;

public class PatrolState : State
{
    public PatrolState(DHAIManager dhAiManager)
    {
        this.DhAiManager = dhAiManager;
    }
    public override void Enter()
    {
        base.Enter();
        DhAiManager.animator.Play("EnemyWalk");
        DhAiManager.SetPatrolInputs();
    }
}
