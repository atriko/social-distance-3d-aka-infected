﻿public class WanderState : State
{
    public float wanderRange;
    public WanderState(DHAIManager dhAiManager)
    {
        this.DhAiManager = dhAiManager;
    }
    public override void Enter()
    {
        base.Enter();
        DhAiManager.animator.Play("EnemyWalk");
        DhAiManager.SetWanderInputs();
    }
}
