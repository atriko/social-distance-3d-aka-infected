﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using KinematicCharacterController;
using KinematicCharacterController.Examples;
using SensorToolkit;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

public enum AIState {
    Idle,
    Wander,
    Patrol,
    Chase,
    Sitting
}
public class DHAIManager : StateMachine, ICharacterController
{
    private PatrolState patrol;
    private List<Vector3> patrolPositions;
    private IdleState idle;
    private ChaseState chase;
    private WanderState wander;
    private CoughState cough;
    private SittingState sitting;

    
    private CompositeDisposable stateObservable;


    public Animator animator;

    public GameObject sneezeParticle;

    public AIState startState;

    [TabGroup("Patrol Settings")] [SerializeField]
    private GameObject[] patrolTargets;

    [TabGroup("Chase Settings")] [SerializeField]
    private GameObject chaseTarget;

    [TabGroup("Wander Settings")] [SerializeField]
    private float wanderRange;

    [TabGroup("Cough Settings")] [SerializeField]
    private float coughDelay;

    [TabGroup("Cough Settings")] [SerializeField]
    private float coughPower;

    [TabGroup("Cough Settings")] [SerializeField]
    private float coughDuration;

    [TabGroup("Cough Settings")] [SerializeField]
    private GameObject infectionArea;

    [TabGroup("Cough Settings")] [SerializeField]
    private RangeSensor rangeSensor;

    [TabGroup("Cough Settings")] [SerializeField]
    private float coughFrequencyMin;

    [TabGroup("Cough Settings")] [SerializeField]
    private float coughFrequencyMax;

    private void ChangeStateInEditor()
    {
        
    }

    private void OnEnable()
    {
        stateObservable = new CompositeDisposable();
        patrol = new PatrolState(this);
        idle = new IdleState(this);
        chase = new ChaseState(this);
        wander = new WanderState(this);
        cough = new CoughState(this);
        sitting = new SittingState(this);
        SetStartState();
    }

    private void SetStartState()
    {
        switch (startState)
        {
            case AIState.Idle:
                Initialize(idle);
                break;
            case AIState.Wander:
                Initialize(wander);
                break;
            case AIState.Patrol:
                Initialize(patrol);
                break;
            case AIState.Sitting:
                Initialize(sitting);
                break;
            case AIState.Chase:
                Initialize(chase);
                break;
        }
    }

    public void StartPatrol()
    {
        if (patrolTargets == null || patrolTargets.Length < 2)
        {
            Debug.Log("Invalid Patrol Points returning Idle");
            SetState(idle);
            return;
        }

        patrolPositions = new List<Vector3>();
        foreach (var point in patrolTargets)
        {
            patrolPositions.Add(point.transform.position);
        }

        SetState(patrol);
    }

    public void StartIdle()
    {
        SetState(idle);
    }

    public void StartSitting()
    {
        SetState(sitting);
    }

    public void StartWander()
    {
        if (wanderRange < 1)
        {
            Debug.Log("Wander range should be greater than one, returning idle state");
            SetState(idle);
            return;
        }
        SetState(wander);
    }

    public void StartCoughing()
    {
        SetState(cough);
    }

    public void StartChase()
    {
        if (chaseTarget == null)
        {
            Debug.Log("No Chase Target has been set. Returning Idle");
            SetState(idle);
            return;
        }

        SetState(chase);
    }

    public void SetPatrolInputs()
    {
        int numberOfStops = patrolPositions.Count - 1;
        int headingPoint = 0;
        bool returning = false;
        Observable.EveryUpdate().Subscribe(delegate
        {
            if (Vector3.Distance(patrolPositions[headingPoint], transform.position) > 0.2f)
            {
                AICharacterInputs inputs = new AICharacterInputs();
                inputs.MoveVector = (patrolPositions[headingPoint] - transform.position).normalized;
                inputs.LookVector = new Vector3(0, 0, inputs.MoveVector.z);
                character.SetInputs(ref inputs);
            }
            else
            {
                Debug.Log("Reached Destination Heading Next Point");
                //TODO: Refactor returning logic
                if (headingPoint == numberOfStops)
                {
                    returning = true;
                }

                if (returning)
                {
                    headingPoint--;
                    if (headingPoint == 0)
                    {
                        returning = false;
                    }
                }
                else
                {
                    headingPoint++;
                }
            }
        }).AddTo(stateObservable);
    }

    public void SetIdleInputs()
    {
        Observable.Timer(TimeSpan.FromSeconds(Random.Range(coughFrequencyMin, coughFrequencyMax)))
            .Subscribe(l => StartCoughing()).AddTo(stateObservable);
        Observable.EveryUpdate().Subscribe(delegate(long l)
        {
            AICharacterInputs inputs = new AICharacterInputs();
            inputs.MoveVector = Vector3.zero;
            inputs.LookVector = Vector3.zero;
            character.SetInputs(ref inputs);
        }).AddTo(stateObservable);
    }

    public void SetChaseInputs()
    {
        Observable.EveryUpdate().Subscribe(delegate(long l)
        {
            AICharacterInputs inputs = new AICharacterInputs();
            inputs.MoveVector = (chaseTarget.transform.position - transform.position).normalized;
            inputs.LookVector.x = inputs.MoveVector.x;
            inputs.LookVector.z = inputs.MoveVector.z;
            character.SetInputs(ref inputs);
        }).AddTo(stateObservable);
    }

    public void SetWanderInputs()
    {
        Vector3 startPosition = transform.position;
        Vector3 randomDirection = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));

        Observable.Timer(TimeSpan.FromSeconds(Random.Range(coughFrequencyMin, coughFrequencyMax)))
            .Subscribe(l => StartCoughing()).AddTo(stateObservable);

        Observable.EveryFixedUpdate().Subscribe(delegate(long l)
        {
            if (Vector3.Distance(startPosition, transform.position) > wanderRange)
            {
                float random = Random.Range(0f, 2 * (float) Math.PI);
                float dirX = (float) Math.Cos(random);
                float dirZ = (float) Math.Sin(random);
                randomDirection =
                    new Vector3(wanderRange * dirX + startPosition.x, 0, wanderRange * dirZ + startPosition.z) -
                    transform.position;
            }

            AICharacterInputs inputs = new AICharacterInputs();
            inputs.MoveVector = randomDirection.normalized;
            inputs.LookVector.x = inputs.MoveVector.x;
            inputs.LookVector.z = inputs.MoveVector.z;
            character.SetInputs(ref inputs);
        }).AddTo(stateObservable);
    }

    public void SetCoughingInputs()
    {
        AICharacterInputs inputs = new AICharacterInputs();
        inputs.MoveVector = Vector3.zero;
        inputs.LookVector = Vector3.zero;
        character.SetInputs(ref inputs);

        float infectionAreaScale = infectionArea.transform.localScale.x;
        float sensorRange = rangeSensor.SensorRange;

        Observable.Timer(TimeSpan.FromSeconds(coughDelay)).Subscribe(delegate(long l)
        {
            //REFACTOR DO TWEEN RETURN ANIMATIONS - POSSIBLE YOYO
            DOTween.To(value => rangeSensor.SensorRange = value, sensorRange,
                sensorRange * coughPower, coughDuration).onComplete = () => DOTween
                .To(value => rangeSensor.SensorRange = value, rangeSensor.SensorRange,
                    sensorRange, coughDuration).onComplete = StartLastState;
            DOTween.To(value => infectionArea.transform.localScale = new Vector3(value, 0.1f, value),
                infectionAreaScale,
                infectionAreaScale * coughPower, coughDuration).onComplete = () =>
                DOTween.To(value => infectionArea.transform.localScale = new Vector3(value, 0.1f, value),
                    infectionArea.transform.localScale.x,
                    infectionAreaScale, coughDuration);
        }).AddTo(stateObservable);
    }

    private void StartLastState()
    {
        if (PreviousState == chase)
        {
            StartChase();
        }
        else if (PreviousState == idle)
        {
            StartIdle();
        }
        else if (PreviousState == cough)
        {
            StartCoughing();
        }
        else if (PreviousState == sitting)
        {
            StartSitting();
        }
        else if (PreviousState == wander)
        {
            StartWander();
        }
        else if (PreviousState == patrol)
        {
            StartWander();
        }
    }

    public void ResetState()
    {
        stateObservable.Dispose();
        stateObservable = new CompositeDisposable();
    }

    private void OnDestroy()
    {
        stateObservable?.Dispose();
    }

    #region MotorFunctions

    public void UpdateRotation(ref Quaternion currentRotation, float deltaTime)
    {
    }

    public void UpdateVelocity(ref Vector3 currentVelocity, float deltaTime)
    {
    }

    public void BeforeCharacterUpdate(float deltaTime)
    {
    }

    public void PostGroundingUpdate(float deltaTime)
    {
    }

    public void AfterCharacterUpdate(float deltaTime)
    {
    }

    public bool IsColliderValidForCollisions(Collider coll)
    {
        throw new NotImplementedException();
    }

    public void OnGroundHit(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint,
        ref HitStabilityReport hitStabilityReport)
    {
    }

    public void OnMovementHit(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint,
        ref HitStabilityReport hitStabilityReport)
    {
    }

    public void ProcessHitStabilityReport(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint,
        Vector3 atCharacterPosition,
        Quaternion atCharacterRotation, ref HitStabilityReport hitStabilityReport)
    {
    }

    public void OnDiscreteCollisionDetected(Collider hitCollider)
    {
    }

    #endregion

    public void SetSittingInputs()
    {
        Observable.Timer(TimeSpan.FromSeconds(Random.Range(coughFrequencyMin, coughFrequencyMax)))
            .Subscribe(l => StartCoughing()).AddTo(stateObservable);
        Observable.EveryUpdate().Subscribe(delegate(long l)
        {
            AICharacterInputs inputs = new AICharacterInputs();
            inputs.MoveVector = Vector3.zero;
            inputs.LookVector = Vector3.zero;
            character.SetInputs(ref inputs);
        }).AddTo(stateObservable);
    }
}