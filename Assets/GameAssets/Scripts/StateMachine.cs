﻿using UnityEngine;

public abstract class StateMachine : MonoBehaviour
{
    protected State CurrentState;
    protected State PreviousState;
    protected DHCharacterController character;

    private void Start()
    {
        character = GetComponent<DHCharacterController>();
    }

    protected void Initialize(State startingState)
    {
        CurrentState = startingState;
        CurrentState.Enter();
    }

    protected void SetState(State newState)
    {
        CurrentState.Exit();
        PreviousState = CurrentState;
        CurrentState = newState;
        CurrentState.Enter();
    }
}