﻿using System.Collections;
using System.Collections.Generic;
using ScriptableObjectArchitecture;
using UnityEngine;

public class IdleState : State
{
    public IdleState(DHAIManager dhAiManager)
    {
        this.DhAiManager = dhAiManager;
    }
    public override void Enter()
    {
        base.Enter();
        DhAiManager.animator.Play("EnemyIdle");
        DhAiManager.SetIdleInputs();
    }
}
