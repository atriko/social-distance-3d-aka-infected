﻿using System;
using ScriptableObjectArchitecture;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class InfectionMeter : MonoBehaviour
{
    [SerializeField] private GameEvent gameLost;
    [SerializeField] private float infectionSpeed;

    public bool isInfecting;
    private IDisposable disposable;
    private Image circle;

    private void Start()
    {
        circle = GameObject.Find("InfectionMeter").GetComponent<Image>(); //REFACTOR
        disposable = Observable.EveryUpdate().Subscribe(delegate
        {
            if (isInfecting)
            {
                circle.fillAmount += infectionSpeed;
                if (Math.Abs(circle.fillAmount - 1) < 0.01f)
                {
                    disposable.Dispose();
                    circle.gameObject.SetActive(false);
                    KillPlayer();
                }
            }
            else
            {
                if (circle.fillAmount < 0.35)
                {
                    return;
                }
                circle.fillAmount -= infectionSpeed;
            }
        } );
    }
    private void KillPlayer()
    {
        gameObject.SetActive(false);
        gameLost.Raise();
    }

}
