﻿using System;
using ScriptableObjectArchitecture;
using UniRx;
using UnityEngine;

namespace GameAssets.Scripts
{
    public class CharacterInputHandler : MonoBehaviour
    {
        [SerializeField] private FloatingJoystick joystick;
        [SerializeField] private GameEvent onPlayerRun;
        [SerializeField] private GameEvent onPlayerIdle;

        private DHCharacterController character;
        private PlayerCharacterInputs characterInputs;
        private IDisposable updateDisposable;
        private bool inputsIsEnable;

        private void Start()
        {
            onPlayerIdle.Raise();
            FindRequiredProperties();
        }

        private void FindRequiredProperties()
        {
            if (!character) character = GetComponent<DHCharacterController>();
            if (!joystick) joystick = FindObjectOfType<FloatingJoystick>();
        }

        public void OnGameStart()
        {
            joystick.OnPointerUp(null);
            updateDisposable?.Dispose();
            updateDisposable = Observable.EveryUpdate().Subscribe(OnUpdate).AddTo(this);
        }

        public void OnGameEnded()
        {
            updateDisposable?.Dispose();
            characterInputs.MoveAxisForward = 0f;
            characterInputs.MoveAxisRight = 0f;
            character.SetInputs(ref characterInputs);
        }

        private void OnUpdate(long obj)
        {
            if (joystick != null) HandleCharacterInput();
        }

        private void HandleCharacterInput()
        {
            if (Math.Abs(joystick.Vertical) < 0.1f || Math.Abs(joystick.Vertical) < 0.1f)
            {
                onPlayerIdle.Raise();
            }
            else
            {
                onPlayerRun.Raise();
            }

            characterInputs.MoveAxisForward = joystick.Vertical * 2f;
            characterInputs.MoveAxisRight = joystick.Horizontal * 2f;
            character.SetInputs(ref characterInputs);
        }
    }
}