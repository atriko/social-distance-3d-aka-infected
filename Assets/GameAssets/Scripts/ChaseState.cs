﻿using UnityEngine;

public class ChaseState : State
{
    public ChaseState(DHAIManager dhAiManager)
    {
        this.DhAiManager = dhAiManager;
    }
    public override void Enter()
    {
        base.Enter();
        DhAiManager.animator.Play("EnemyWalk");
        DhAiManager.SetChaseInputs();
    }
}
