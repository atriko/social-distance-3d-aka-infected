﻿public class CoughState : State
{
    public CoughState(DHAIManager dhAiManager)
    {
        this.DhAiManager = dhAiManager;
    }
    public override void Enter()
    {
        base.Enter();
        DhAiManager.animator.Play("EnemySneeze");
        DhAiManager.sneezeParticle.SetActive(true);
        DhAiManager.SetCoughingInputs();
    }

    public override void Exit()
    {
        base.Exit();
        DhAiManager.sneezeParticle.SetActive(false);
    }
}
