﻿public class SittingState : State
{
    public SittingState(DHAIManager dhAiManager)
    {
        this.DhAiManager = dhAiManager;
    }
    public override void Enter()
    {
        base.Enter();
        DhAiManager.animator.Play("EnemySit");
        DhAiManager.SetSittingInputs();
    }
}
