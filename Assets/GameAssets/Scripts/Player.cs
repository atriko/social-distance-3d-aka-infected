﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private Image infectionMeter;

    private IDisposable infectionCounterDisposable;
    private int nearInfectedCount;
    private void Start()
    {
        infectionCounterDisposable = Observable.EveryFixedUpdate().Select(l => nearInfectedCount > 0).Subscribe(
            delegate(bool inInfectedArea)
            {
                if (inInfectedArea)
                {
                    StartInfectionTimer();
                }
                else
                {
                    StopInfectionTimer();
                }
            });
    }
    public void IncreaseInfectedCount()
    {
        nearInfectedCount++;
    }
    public void DecreaseInfectedCount()
    {
        nearInfectedCount--;
    }
    
    private void StartInfectionTimer()
    {
        infectionMeter.GetComponent<InfectionMeter>().isInfecting = true;
    }
    private void StopInfectionTimer()
    {
            infectionMeter.GetComponent<InfectionMeter>().isInfecting = false;
    }

    public void DisposeInfectionCounter()
    {
        StopInfectionTimer();
        infectionCounterDisposable?.Dispose();

    }
    private void OnDestroy()
    {
        DisposeInfectionCounter();
    }
}
