﻿
using System;
using DG.Tweening;
using ScriptableObjectArchitecture;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    [SerializeField] private GameObject leftDoor;
    [SerializeField] private GameObject rightDoor;
    
    [SerializeField] private GameEvent onGameWon;

    private Vector3 leftDoorClosedPosition;
    private Vector3 rightDoorClosedPosition;
    private void Start()
    {
        leftDoorClosedPosition = leftDoor.transform.position;
        rightDoorClosedPosition = rightDoor.transform.position;
    }

    public void OpenDoors()
    {
        leftDoor.transform.DOMove(leftDoor.transform.position - new Vector3(2f, 0, 0), 0.6f);
        rightDoor.transform.DOMove(rightDoor.transform.position + new Vector3(2f, 0, 0), 0.6f);
    }

    public void CloseDoors()
    {
        leftDoor.transform.DOMove(leftDoorClosedPosition, 0.6f);
        rightDoor.transform.DOMove(rightDoorClosedPosition, 0.6f);
    }

    public void ProceedToNextLevel()
    {
        leftDoor.transform.DOMove(leftDoorClosedPosition, 0.3f);
        rightDoor.transform.DOMove(rightDoorClosedPosition, 0.3f).onComplete = () => onGameWon.Raise();
    }
}
