﻿using UnityEngine;

public abstract class State
{
    protected DHAIManager DhAiManager;
    public virtual void Enter()
    {
    }
    public virtual void Exit()
    {
        DhAiManager.ResetState();
    }
}